var useLocalStorage = false;

function switchUseLS(){
  useLocalStorage = !useLocalStorage;
}

function isOnline() {
  return window.navigator.onLine;
}


// REST
class ServerService {
  async sendToServer(data) {
    try {
      await fetch('/news', {
        method: 'post',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(data),
      });
    } catch (error) {
      console.error('Cannot fetch data: ', error);
    }
  }

 async getFromServer() {
    try {
      const data = await fetch('/news/all');
      return data.text();
    } catch (error) {
      console.error('Cannot fetch data: ', error);
    }
  }
}
//

const newsContainer = document.getElementById('content_news');

class News{
  constructor(body, picture){
    this.body = body;
    this.picture = picture;
  }
}

function newsTemplate(news) {
var body = news.body;
var picture = news.picture;



return `
<ul>
    <li class="col-sm-3" class="big" class="navbar-brand" style="color: black;text-decoration: none;list-style: none">
    <a href="#" >

        <img src="${picture}" width="100%">
        <br><br> 
        <p class="shrft">${body}</p>
        </a>
      </li>
      </ul>
`
}


const service = new ServerService();

const initAndRenderData = async () => {
  const items = await service.getFromServer();
  console.log(items);

  const itemsStringified = JSON.stringify(items);

  JSON.parse(items).forEach(({body, picture }) => {
         var tempNews = new News(body, picture);
         $('#content_news').append(
           newsTemplate(tempNews),
         );
   });
}

const onOnline = () => {
  initAndRenderData();
  console.log('Network status: online');
}

const onOffline = () => {
  console.log('Connection lost');
}

window.addEventListener('online', onOnline);
window.addEventListener('offline', onOffline);
window.addEventListener('DOMContentLoaded', initAndRenderData);

