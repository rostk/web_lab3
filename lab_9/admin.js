

var useLocalStorage = false;

function switchUseLS(){
  useLocalStorage = !useLocalStorage;
}

function isOnline() {
  return window.navigator.onLine;
}
const input_form = document.getElementById('addPicture');
const newsForm = document.getElementById('newsForm')
const text = document.getElementById('text');
const picture_Input = document.getElementById('formForFile');

class News{
  constructor(body, picture){
    this.body = body;
    this.picture = picture;
  }
}
const onSubmitPress = (e) => {
  e.preventDefault();

  const isValid = (text.value.length > 0 );
  var news = new News(text.value, picture_Input.value);

  if (!isValid) return;
  if (!isOnline()) {
    writeInto(news);
  } else {
    console.log('isOnline');
  }


  input_form.reset();
  newsForm.reset();

  alert('Вашу новину успішно збережено!');
}

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#example_picture').attr('src', e.target.result);             //result property returns the file's contents. This property is only valid after the read operation is complete                                                                             
      }                                                              //attr() достаёт значение атрибута первого элемента, который ему встретится
    

    reader.readAsDataURL(input.files[0]);
  }
}

$("#formForFile").change(function() {
  readURL(this);
});




function writeNews() {
  var news = [];
  var news_item = localStorage.getItem('news_add');
  if (news_item !== null) {
      news = JSON.parse(news_item); 
  }
  return news;
}

function writeInto(newsItem) {
  if(useLocalStorage){
    var news = writeNews();
      news.push(newsItem);
      localStorage.setItem('news_add', JSON.stringify(news));
      return false;
  } 
else{
  var openDB = indexedDB.open("news_add", 1);

  openDB.onerror = function(event) {
    alert("Error occurred when loading news");
  };
  openDB.onupgradeneeded = function() {
      var db = openDB.result;
      var store = db.createObjectStore("news", {keyPath: "body"});
      store.createIndex("body", "body");
      store.createIndex("picture", "picture");
  };
  openDB.onsuccess = function(event) {
    var db = openDB.result;
    var tx = db.transaction(["news"], "readwrite");
    var store = tx.objectStore("news");
    var addFeedback = store.put(newsItem);
    addFeedback.onsuccess = function(event){
    }
    addFeedback.onerror = function(event){
      alert("Error occurred when loading news");
    }
    tx.oncomplete = function(){
      db.close();
    }
  };
}
};



const addButton = document.getElementById('submit-btn');
addButton.onclick = onSubmitPress;
